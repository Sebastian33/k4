
/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   // hashcode:
   // https://stackoverflow.com/questions/113511/best-implementation-for-hashcode-method
   // fractions:
   // https://codippa.com/how-to-work-with-fractions-in-java/
   // Greatest common denominator:
   // https://stackoverflow.com/questions/6618994/simplifying-fractions-in-java


   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction f1 = new Lfraction (-2, 5);
      Lfraction f2 = new Lfraction (-4, 7);

      System.out.println(f1.toString());
      System.out.println(f2.toString());
   }

   private final long numerator;
   private final long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {

      if (b != 0) {
         long gcd = gcd(a, b);
            if ((b / gcd) < 0) {
               numerator = a / gcd * -1;
               denominator = b / gcd * -1;
            } else {
               numerator = a / gcd;
               denominator = b / gcd;
            }

      } else {
         throw new RuntimeException("Denominator cannot be 0!");
      }
   }

   /**
    * Greatest common denominator.
    * @param a numerator
    * @param b denominator
    * @return gcd
    */
   private static long gcd(long a, long b) {
      return b == 0 ? a : gcd(b, a % b);
   }

   /**
    * Returns simplified fraction.
    * @param a numerator
    * @param b denominator
    * @return simplified fraction
    */
   private Lfraction lowest(long a, long b) {

      long gcd = gcd(a, b);
      return new Lfraction(a / gcd, b / gcd);
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return String.format("%s/%s", numerator, denominator);
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      return compareTo((Lfraction) m) == 0;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      Lfraction thisReduced = this.lowest(this.numerator, this.denominator);

      int result = (int) (thisReduced.getNumerator() ^ (thisReduced.getNumerator() >>> 32));

      result = 31 * result + (int)(thisReduced.getDenominator() ^ (thisReduced.getDenominator() >>> 32));

      return result;
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {

      long sumDenominator = gcd(denominator, m.getDenominator());
      sumDenominator = (denominator * m.getDenominator() / sumDenominator);
      long sumNumerator = (numerator) * (sumDenominator / denominator) +
              (m.getNumerator()) * (sumDenominator/ m.getDenominator());
      return lowest(sumNumerator, sumDenominator);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long timesDenom = (denominator * m.getDenominator());
      long timesNum = (numerator * m.getNumerator());
      return lowest(timesNum, timesDenom);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (numerator == 0) {
         throw new RuntimeException("Inversion creates zero division!");
      }
      return lowest(denominator, numerator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return lowest(numerator * -1, denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      return times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      long result = numerator * m.getDenominator() - denominator * m.getNumerator();
      if (result == 0) {
         return 0;
      }
      return (result > 0) ? 1 : -1;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.numerator, this.denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return numerator / denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      long integerPart = this.integerPart();
      Lfraction integerFraction = new Lfraction(integerPart, 1);
      return this.minus(integerFraction);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      Double num = (double) numerator;
      Double denom = (double) denominator;
      return num / denom;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      long fractionNumerator = Math.round(f * d);
      return new Lfraction(fractionNumerator, d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      if (s.isEmpty()) {
         throw new RuntimeException("Given string is empty!" + s);
      }
      String[] values = s.split("/");
      if (values.length != 2) {
         throw new RuntimeException("Fraction must contain only two numbers! Given Fraction was: " + s);
      }
      try {
         return new Lfraction(Long.parseLong(values[0]), Long.parseLong(values[1]));
      } catch (NumberFormatException e) {
         throw new RuntimeException("Given fraction must contain whole numbers! Given fraction was: " + s);
      }
   }

   public Lfraction pow (int exponent) {

      Lfraction result = new Lfraction(this.numerator, this.denominator);

      if (exponent == 0) {
         result = new Lfraction(1, 1);
         //System.out.println(result);

      } else if (exponent == 1) {
         result = new Lfraction(this.numerator, this.denominator);
         //System.out.println(result);

      } else if (exponent == -1) {
         result = result.inverse();
         //System.out.println(result);

      } else if (exponent < 0) {
         result = result.pow(-exponent).inverse();
         //System.out.println(result);

      } else {
         result = result.times(result.pow(exponent - 1));
         //System.out.println(result);
      }

      return result.lowest(result.getNumerator(), result.getDenominator());
   }
}

